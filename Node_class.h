/* ************************************************************************
Name: Lanlan Liu
Assignment: code05
Purpose: BST
Notes:When using recursion, why does some function return data in last
 recursion(ex. getEntry()) while some function return data in first
 recursion(ex.remove())?
************************************************************************ */

#ifndef CODE05_NODE_CLASS_H
#define CODE05_NODE_CLASS_H
#include <istream>
#include <iostream>



using namespace std;

struct Node{
    int id;
    string data;
    Node *left;
    Node *right;
    Node();
};
#endif //CODE05_NODE_CLASS_H
