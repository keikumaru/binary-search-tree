/* ************************************************************************
Name: Lanlan Liu
Assignment: code05
Purpose: BST
Notes:When using recursion, why does some function return data in last
 recursion(ex. getEntry()) while some function return data in first
 recursion(ex.remove())?
************************************************************************ */


#include "Main.h"
int main(int argc, char ** argv) {
        //g++ -I ./ *.cpp -o promp
        // promp
        cout << "Enter a number between 0 - 20: " << endl;
        if (argc >= 2) {
                int num = atoi(argv[1]);
                if (num < 0 || num > 20) {
                        cout << "Invalid input, please restart the program." << endl;
                        return 1;
                }
                BST *newBst = new BST();
                vector<int> data;
                for (int i = 0; i < num; i++) {
                        int num = newBst->randomNum(data);
                        newBst->addNode(num);
                }
                cout << "Check if the list is empty, 0 is false, 1 is true: " << newBst->isEmpty() << endl;
                cout << "The height of the tree: " << newBst->getHeight() << endl;
                cout << "number of nodes: " << newBst->getNumberOfNodes() << endl;
                cout << "head is: " << newBst->getHead() << endl;
                newBst->addNode(800);//I need a specific number to run some funtions
                cout << "The string entry of the node with id 800: " << newBst->getEntry(800) << endl;
                cout << "Check whether the tree contains 800, 0 is false, 1 is true: " << newBst->contains(800) << endl;
                cout << "print the tree in preorder traverse: " << endl;
                newBst->preorderTraverse();
                cout << "Remove node 800..." << endl;
                newBst->remove(800);
                cout << "print the tree after removal using inorder traverse: " << endl;
                newBst->inorderTraverse();
                cout << "print the tree using postorder traverss: " << endl;
                newBst->postorderTraverse();
                cout << "clear all the nodes..." << endl;
                newBst->clear();
                cout << "print the tree after clear in preorder traverse: " << endl;
                newBst->preorderTraverse();
                return 0;
        } else {
                cout << "Listing all parameters, including the program name... " << endl;
        }
}
