/* ************************************************************************
Name: Lanlan Liu
Assignment: code05
Purpose: BST
Notes:When using recursion, why does some function return data in last
 recursion(ex. getEntry()) while some function return data in first
 recursion(ex.remove())?
************************************************************************ */
#include "BST_class.h"
void BST::setHeight_left(int num){
    this->height_left = num;
};
int BST::getHeight_left(){
    return this->height_left;
};

void BST::setHeight_Right(int num){
    this->height_right = num;
};
int BST::getHeight_Right(){
    return this->height_right;
};

void BST::setCount(int num){
    this->count = num;
};
int BST::getCount(){
    return this->count;
};

bool BST::isEmpty() {
    if (root == nullptr){
        return true;
    }
    return false;
}

void*BST::addNode(int num){
    this->root = addNode(root,num);
}
Node* BST::addNode(Node* root, int val) {
   if (root == nullptr) {
        Node *newNode = new Node();
        newNode->id = val;
        newNode->data = "Number " + to_string(val);
        if (this->root == nullptr){
            this->root = newNode;
        }
        setCount(getCount()+1);
        return newNode;
    }
    if (root->id > val) {
        root->left = addNode(root->left, val);
    } else {
        root->right = addNode(root->right, val);
    }
    return root;
}

int BST::getNumberOfNodes() {
    return getCount();
}

int BST::getHead() {
    return this->root->id;
}

string BST::getEntry(int id){
    return getEntry(root,id);
}
string BST::getEntry(Node * base,int id) {
    if (base == nullptr){
        return "";
    }
    if (base->id < id){
        return getEntry(base->right,id);
    }
    if(base->id > id){
        return getEntry(base->left,id);
    }
    return base->data;
}

Node* BST::clear() {
    this->root = clear(root);
}
Node* BST::clear(Node * base) {
    if(base == nullptr){
        return base;
    }
    else{
        clear(base->left);
        clear(base->right);
        delete base;
        base = nullptr;
    }
    return base;
}

void BST::preorderTraverse(){
    preorderTraverse(root);
}
void BST::preorderTraverse(Node * base) {
        if(base == nullptr){
            return;
        }
        cout<< base->id<<endl;
        preorderTraverse(base->left);
        preorderTraverse(base->right);
}

void BST::inorderTraverse(){
    inorderTraverse(root);
}
void BST::inorderTraverse(Node * base) {
    if(base == nullptr){
        return;
    }
    inorderTraverse(base->left);
    cout<<base->id<<endl;
    inorderTraverse(base->right);
}

void BST::postorderTraverse(){
    postorderTraverse(root);
}
void BST::postorderTraverse(Node * base) {
    if(base == nullptr){
        return;
    }
    postorderTraverse(base->right);
    postorderTraverse(base->left);
    cout<<base->id<<endl;
}

bool BST::contains(int num) {
    return contains(root,num);
}
bool BST::contains (Node * base,int num){
    if( base== nullptr){
        return false;
    }
    if (base->id == num){
        return true;
    }
    if(contains(base->right,num) || contains(base->left,num)){
        return true;
    }
    return false;
}

int BST::getHeight(){
    return getHeight(root);
}
int BST::getHeight(Node *base) {
    if( base== nullptr){
        return 0;
    }
    if(base->left){
        setHeight_left(getHeight_left()+1);
        getHeight(base->left);
    }
    if(base->right){
        setHeight_Right(getHeight_Right()+1);
        getHeight(base->right);
    }
    if(getHeight_left() >= getHeight_Right()){
        return getHeight_left();
    }
    return getHeight_Right();
}

Node *BST::remove(Node* base, int num) {
    if (base == nullptr){
        return base;
    }
    else if (num < base->id){
        base->left = remove(base->left,num);
    }
    else if(num > base->id){
        base->right = remove(base->right,num);
    }
    else{
        if (base->left == nullptr && base->right == nullptr){
            delete base;
            base = nullptr;
        }
        else if(base->left == nullptr){
            Node * temp = base;
            base = base->right;
            delete temp;
        }
        else if (base->right == nullptr){
            Node *temp = base;
            base = base->left;
            delete temp;
        }
        else{
            Node * temp = findMin(base->right);
            base->id = temp->id;
            base->data = temp->data;
            base->right = remove(base->right,temp->id);
        }
    }
    return base;
}
void BST::remove(int num){
    this->root = remove(root,num);
}

Node* BST::findMin(Node *base) {
    if (base == nullptr){
        return nullptr;
    }
    while (base->left != nullptr){
        base = base->left;
    }
    return base;
}

int BST::randomNum(vector<int> &data)  {
    srand(time(0));
    int newNum = rand()%888+100;
    for (int i : data){
        if (i == newNum){
            newNum = rand()%888+100;
        }
    }
    data.push_back(newNum);
    return newNum;
}