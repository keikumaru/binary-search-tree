/* ************************************************************************
Name: Lanlan Liu
Assignment: code05
Purpose: BST
Notes:When using recursion, why does some function return data in last
 recursion(ex. getEntry()) while some function return data in first
 recursion(ex.remove())?
************************************************************************ */

#ifndef CODE05_BST_CLASS_H
#define CODE05_BST_CLASS_H
#include "Node_class.h"
#include <string>
#include <ctime>
#include <vector>
using namespace std;
class BST{
private:
    int height_left = 0;
    int height_right = 0;
    int count = 0;
    Node * root = nullptr;
    Node* clear(Node *);
    void preorderTraverse(Node *);
    Node *addNode(Node *,int);
    string getEntry(Node*,int);
    void inorderTraverse(Node *);
    void postorderTraverse(Node *);
    bool contains(Node*,int);
    int getHeight(Node*);
public:
    int randomNum(vector<int>&);
    void setHeight_left(int);
    int getHeight_left();
    void setHeight_Right(int);
    int getHeight_Right();
    void setCount(int);
    int getCount();
    bool isEmpty();
    void *addNode(int);
    int getHeight();
    int getNumberOfNodes();
    int getHead();
    string getEntry(int);
    Node* remove(Node*,int);
    void remove(int);
    Node* clear();
    bool contains(int);
    void preorderTraverse();
    void inorderTraverse();
    void postorderTraverse();
    Node* findMin(Node*);
};
#endif //CODE05_BST_CLASS_H
