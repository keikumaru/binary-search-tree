/* ************************************************************************
Name: Lanlan Liu
Assignment: code05
Purpose: BST
Notes:When using recursion, why does some function return data in last
 recursion(ex. getEntry()) while some function return data in first
 recursion(ex.remove())?
************************************************************************ */

#ifndef CODE05_MAIN_H
#define CODE05_MAIN_H

#include "Node_class.h"
#include "BST_class.h"
#include <vector>

#endif //CODE05_MAIN_H
